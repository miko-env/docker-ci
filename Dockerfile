FROM archlinux:base-devel
RUN pacman -Syu git --needed --noconfirm
RUN useradd -m builder ;\
    echo "builder ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers ;\
    sudo -u builder git clone https://gitlab.com/miko-env/pkgbuilds.git /home/builder/miko-pkgbuilds ;\
    cd /home/builder/miko-pkgbuilds ;\
    sudo -u builder ./build.sh testing -i ;\
    find -iname '*.pkg.tar.zst' -exec rm -v {} \;
WORKDIR /home/builder
